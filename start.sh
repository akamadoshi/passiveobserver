docker kill nginx mariadb matomo
docker rm nginx mariadb matomo
docker-compose \
  -p passiveobserver \
  -f ${PWD}/matomo/docker-compose.yml \
  -f ${PWD}/mariadb/docker-compose.yml \
  -f ${PWD}/nginx/docker-compose.yml \
  up -d \
  --force-recreate \
  --build

