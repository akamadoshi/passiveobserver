docker-compose run --rm --entrypoint "\
	certbot certonly --webroot -w /var/www/certbot \
	--staging \
	--email passiveobserver@d.passibeobserver.com \
	--rsa-key-size 4096 \
	--agree-tos \
        -d posabit.passiveobserver.com \
	--force-renewal" certbot
